/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.lobos.springrestmongotest.books.service;

import java.util.Collection;
import java.util.Optional;
import mx.com.lobos.springrestmongotest.books.dao.ContactsDao;
import mx.com.lobos.springrestmongotest.entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author antoniob
 */
@Service("ContacLobo")
public class ContactServiceImp implements ContactService {

    @Autowired
    private ContactsDao contacDao;

    @Override
    public String testContact() {
        return "Hola mundo soy una prueba";
    }

    @Override
    public Collection<Contact> getContacts() {
        return contacDao.findAll();
    }

    @Override
    public Optional<Contact> getContactById(String id) {
        return contacDao.findById(id);
    }

    @Override
    public Optional<Contact> deleteContactById(String id) {
        Optional<Contact> contac = contacDao.findById(id);
        contac.ifPresent(b -> contacDao.delete(b));
        return contac;
    }

    @Override
    public Contact insertContact(Contact contact) {
        return contacDao.insert(contact);
    }

    @Override
    public Optional<Contact> updateContact(String id, Contact contact) {
        Optional<Contact> contactUpdate = contacDao.findById(id);
        contactUpdate.ifPresent(b -> b.setName(contact.getName()));
        contactUpdate.ifPresent(b -> b.setPhone(contact.getPhone()));
        contactUpdate.ifPresent(b -> b.setAddressLines(contact.getAddressLines()));
        return contactUpdate;
    }

    @Override
    public Collection<Contact> getContactsInOrder() {
            return contacDao.getContactsInOrder();
    }

}
