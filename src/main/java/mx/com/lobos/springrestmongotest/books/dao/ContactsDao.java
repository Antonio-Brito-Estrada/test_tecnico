/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.lobos.springrestmongotest.books.dao;

import java.util.List;
import mx.com.lobos.springrestmongotest.entities.Contact;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author hp
 */
public interface ContactsDao extends MongoRepository<Contact, String>{
   
    @Aggregation("{'$sort' : { 'name' : 1.0 }}")
    public List<Contact> getContactsInOrder();
}
