/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.lobos.springrestmongotest.books.service;

import java.util.Collection;
import java.util.Optional;
import mx.com.lobos.springrestmongotest.entities.Contact;

/**
 *
 * @author antoniob
 */
public interface ContactService {
    
    public String testContact();
    
    public Collection<Contact> getContacts();

    public Optional<Contact> deleteContactById(String id);
    
    public Optional<Contact> getContactById(String id);
    
    public Contact insertContact(Contact contact);
    
    public Optional<Contact> updateContact(String id, Contact contact); 
    
    public Collection<Contact> getContactsInOrder();
    
}
