/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.lobos.springrestmongotest.books.controller;

import java.util.Collection;
import java.util.Optional;
import mx.com.lobos.springrestmongotest.books.service.ContactService;
import mx.com.lobos.springrestmongotest.entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author antoniob
 */
@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@RequestMapping("/contacts")
public class ContactsController {
    
    @Autowired
    @Qualifier("ContacLobo")
    ContactService ContactService;
    
    
    @GetMapping
    public String test() {
        return ContactService.testContact();
    }
    
    @GetMapping("/getContacts")
    public Collection<Contact> getContacts() {
        return ContactService.getContactsInOrder();
    }
    
    @CrossOrigin(origins = "*")
    @DeleteMapping(value = "/deleteContact/{id}")
    public Optional<Contact> deleteContactById(
           @PathVariable("id") String id
    ) {
        return ContactService.deleteContactById(id);
    }
    
    @GetMapping("/getContact/{id}")
    public Optional<Contact> getContactById(
           @PathVariable("id") String id
    ) {
        return ContactService.getContactById(id);
    }
    
    @CrossOrigin(origins = "*")
    @PostMapping(value = "/{addContact}")
    public Contact addContact(
           @RequestBody Contact contact
    ) {
    return ContactService.insertContact(contact);
    }

    @CrossOrigin(origins = "*")
    @PutMapping(value = "/{updateBook}/{id}")
    public Optional<Contact> updateBookById(
            @PathVariable("id") String id,
            @RequestBody Contact contact
    ) {
    return ContactService.updateContact(id, contact);
    }
}
