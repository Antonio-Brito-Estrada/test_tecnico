package mx.com.lobos.springrestmongotest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class SpringRestMongoTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRestMongoTestApplication.class, args);
	}

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowCredentials(true)
                        .allowedMethods("*")
                        .allowedHeaders("ds",
                                "Authorization",
                                "Access-Control-Allow-Origin",
                                "Access-Control-Allow-Headers",
                                "Origin,Accept",
                                "X-Requested-With",
                                "Content-Type",
                                "Access-Control-Request-Method",
                                "Access-Control-Request-Headers");
            }
        };
    }
}
